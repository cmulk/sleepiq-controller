import os
import sys
import logging
import signal
import json
from time import sleep
from dotenv import load_dotenv
import paho.mqtt.client as mqtt
from sleepyq import Sleepyq

load_dotenv(override=True)

MQTT_ID = os.getenv("MQTT_ID")
MQTT_BROKER = os.getenv("MQTT_BROKER")
MQTT_PORT = int(os.getenv("MQTT_PORT"))
MQTT_USERNAME = os.getenv("MQTT_USERNAME")
MQTT_PASSWORD = os.getenv("MQTT_PASSWORD")

SLEEPIQ_USERNAME = os.getenv("SLEEPIQ_USERNAME")
SLEEPIQ_PASSWORD = os.getenv("SLEEPIQ_PASSWORD")
BED_ID = os.getenv("BED_ID")


# MQTT Topics
AVAILABLE_TOPIC = os.getenv("AVAILABLE_TOPIC")
BED_STATUS = os.getenv("BED_STATUS")

SET_LEFT = os.getenv("SET_LEFT")
SET_RIGHT = os.getenv("SET_RIGHT")

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s | %(name)s | %(levelname)s | %(message)s"
)

logger = logging.getLogger("sleepiq-controller")


def end_well(signum, stackframe):
    logger.info("Stopping MQTT loop...")
    client.publish(AVAILABLE_TOPIC, "offline")
    client.disconnect()
    client.loop_stop()
    sys.exit()


def on_log(client, userdata, level, buf):
    logger.info("mqtt: %s", buf)


def on_set_left_number(client, userdata, msg):

    try:
        # sleepyc.login()
        if "favorite" in str(msg.payload):
            number = int(get_sleep_numbers(BED_ID)["left_fav"])
        else:
            number = int(msg.payload)
        if not 5 <= number <= 100:
            raise Exception(f"Invalid sleep number {msg.payload}")

        sleepyc.set_sleepnumber("left", number, bedId=BED_ID)
        logger.info("Set left sleepnumber to %s", number)

    except Exception as exc:
        logger.error("Error setting sleep number: %s", repr(exc))


def on_set_right_number(client, userdata, msg):
    try:
        # sleepyc.login()
        if "favorite" in str(msg.payload):
            number = int(get_sleep_numbers(BED_ID)["right_fav"])
        else:
            number = int(msg.payload)
        if not 5 <= number <= 100:
            raise Exception(f"Invalid sleep number {msg.payload}")

        sleepyc.set_sleepnumber("right", number, bedId=BED_ID)
        logger.info("Set right sleepnumber to %s", number)

    except Exception as exc:
        logger.error("Error setting sleep number: %s", repr(exc))


def on_connect(client, userdata, flags, rc):
    client.subscribe(SET_LEFT)
    client.subscribe(SET_RIGHT)
    client.message_callback_add(SET_LEFT, on_set_left_number)
    client.message_callback_add(SET_RIGHT, on_set_right_number)


def get_sleep_numbers(bed_id):

    # sleepyc.login()

    for bed in sleepyc.beds_with_sleeper_status():
        if bed.data["bedId"] == bed_id:
            thebed = bed
            break

    try:
        favs = sleepyc.get_favsleepnumber(bedId=BED_ID)
        return {
            "name": thebed.data["name"],
            "left": thebed.left.data["sleepNumber"],
            "left_fav": favs.left,
            "right": thebed.right.data["sleepNumber"],
            "right_fav": favs.right,
        }

    except UnboundLocalError:
        raise Exception(f"Bed {bed_id} not found!")


if __name__ == "__main__":
    logger.info("Starting...")

    signal.signal(signal.SIGINT, end_well)
    signal.signal(signal.SIGTERM, end_well)

    # Login to SleepIQ
    sleepyc = Sleepyq(SLEEPIQ_USERNAME, SLEEPIQ_PASSWORD)

    client = mqtt.Client(client_id=MQTT_ID)
    client.on_log = on_log

    client.username_pw_set(MQTT_USERNAME, MQTT_PASSWORD)
    client.tls_set()
    client.on_connect = on_connect

    client.connect(MQTT_BROKER, port=MQTT_PORT)
    client.loop_start()

    sleep(2)

    while True:
        client.publish(AVAILABLE_TOPIC, "online")

        # client.publish(BED_STATUS, json.dumps(get_sleep_numbers(BED_ID)))
        # from rich.pretty import pprint
        # pprint(get_sleep_numbers(BED_ID))

        sleep(3600)
